package it.unibo.oop.lab.enum1;



import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
   
    	final SportSocialNetworkUserImpl<User> dcassani = new SportSocialNetworkUserImpl<User>("davide", "cassani", "dcassani", 53); 
        final SportSocialNetworkUserImpl<User> becclestone = new SportSocialNetworkUserImpl<User>("bernie", "ecclestone", "becclestone", 83); 
        final SportSocialNetworkUserImpl<User> falonso = new SportSocialNetworkUserImpl<User>("fernando", "alonso", "falonso", 34);
        
        falonso.addSport(Sport.F1);
        falonso.addSport(Sport.SOCCER); 
        
        System.out.println(falonso.hasSport(Sport.F1));
        
    }
    
    

}
